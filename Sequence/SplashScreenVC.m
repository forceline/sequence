//
//  SplashScreenVC.m
//  Sequence
//
//  Created by Artur on 13.10.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import "SplashScreenVC.h"
#import "HelpPageVC.h"
#import "HelpVC.h"

@interface SplashScreenVC () <UIPageViewControllerDataSource>

@end

@implementation SplashScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    HelpPageVC *vc = [self createHelpPageVC];
    
    [self addChildViewController:vc];
    
    [self.view addSubview:vc.view];
    
    [vc didMoveToParentViewController:self];
    
    CGRect pageViewRect = self.view.bounds;
    vc.view.frame = pageViewRect;
 
    self.automaticallyAdjustsScrollViewInsets = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (HelpPageVC *)createHelpPageVC {
    
    HelpPageVC *result = [[HelpPageVC alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    HelpVC *vc = [[HelpVC alloc] init];
    vc.helpPageVC = result;
    vc.pageIndex = 0;
    vc.helpPageVC.pageControlView.currentPage = 0;
    
    
    [result setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
        
    }];
    
    result.dataSource = self;
    
    return result;

}

#pragma mark - UIPageViewControllerDataSource

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    UIViewController *result = nil;
    
    HelpVC *currentVC = (HelpVC *)viewController;
    NSInteger currentPageIndex = [currentVC pageIndex];
    
    if (currentPageIndex > 0) {
        HelpVC *vc = [[HelpVC alloc] init];
        vc.helpPageVC = (HelpPageVC *)pageViewController;
        vc.pageIndex = currentPageIndex - 1;
        
        result = vc;
    }
    
    return result;
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    UIViewController *result = nil;
    
    NSInteger pageCount = [(HelpPageVC *)pageViewController pageCount];
    
    HelpVC *currentVC = (HelpVC *)viewController;
    NSInteger currentPageIndex = [currentVC pageIndex];
    
    if (currentPageIndex + 1 < pageCount) {
        HelpVC *vc = [[HelpVC alloc] init];
        vc.helpPageVC = (HelpPageVC *)pageViewController;
        vc.pageIndex = currentPageIndex + 1;

        result = vc;
    }
    
    return result;
    
}

@end
