//
//  PrefHelper.h
//  Sequence
//
//  Created by Artur on 12.10.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrefHelper : NSObject

+ (instancetype)shared;

- (void)setSkipHelpOnLoad:(BOOL)value;
- (BOOL)getSkipHelpOnLoad;

- (void)setSequenceTextFieldValue:(NSString *)text;
- (NSString *)getSequenceTextFieldValue;

@end
