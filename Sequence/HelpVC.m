//
//  HelpVC.m
//  Sequence
//
//  Created by Artur on 12.10.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import "HelpVC.h"
#import "PrefHelper.h"

@interface HelpVC ()

@end

@implementation HelpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setAppearence];
    [self loadData];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.helpPageVC.pageControlView.currentPage = self.pageIndex;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setAppearence {
    self.hideHelpButton.hidden = (self.pageIndex != self.helpPageVC.pageCount - 1);
}

- (void)loadData {
    
    NSDictionary *data = [self.helpPageVC getDataForPageIndex:self.pageIndex];
    UIImage *image    = [data objectForKey:@"image"];
    NSData  *textData = [data objectForKey:@"text"];
    
    if (image) {
        self.imageView.image = image;
    }
    if (textData) {
        self.helpTextLabel.text = [[NSString alloc] initWithData:textData encoding:NSUTF8StringEncoding];
    }

}

- (IBAction)hideHelpButtonDidTap:(id)sender {
    [[PrefHelper shared] setSkipHelpOnLoad:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
