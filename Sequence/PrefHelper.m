//
//  PrefHelper.m
//  Sequence
//
//  Created by Artur on 12.10.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import "PrefHelper.h"

@implementation PrefHelper

NSString *SkipHelpOnLoadKey         = @"SkipHelpOnLoadKey";
NSString *SequenceTextFieldValueKey = @"SequenceTextFieldValueKey";

+ (instancetype)shared {
    static PrefHelper *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

-(void)setSkipHelpOnLoad:(BOOL)value {
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:SkipHelpOnLoadKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)getSkipHelpOnLoad {
    BOOL result = [[NSUserDefaults standardUserDefaults] boolForKey:SkipHelpOnLoadKey];
    return result;
}

- (void)setSequenceTextFieldValue:(NSString *)text {
    [[NSUserDefaults standardUserDefaults] setObject:text forKey:SequenceTextFieldValueKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (NSString *)getSequenceTextFieldValue {
    NSString *result = [[NSUserDefaults standardUserDefaults] objectForKey:SequenceTextFieldValueKey];
    return result;
}

@end
