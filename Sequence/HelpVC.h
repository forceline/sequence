//
//  HelpVC.h
//  Sequence
//
//  Created by Artur on 12.10.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelpPageVC.h"

@interface HelpVC : UIViewController

@property (weak, nonatomic) HelpPageVC *helpPageVC;

@property (nonatomic) NSInteger pageIndex;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *helpTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *hideHelpButton;

@end
