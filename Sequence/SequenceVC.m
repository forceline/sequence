//
//  SequenceVC.m
//  Sequence
//
//  Created by Artur on 13.10.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import "SequenceVC.h"
#import "SplashScreenVC.h"
#import "PrefHelper.h"

@interface SequenceVC () <UITextFieldDelegate>
{
    BOOL isFirstAppear;
}

@property (weak, nonatomic) IBOutlet UITextField *sequenceTextField;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@end

@implementation SequenceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isFirstAppear = YES;
    
    [self resetResult];
    
    self.sequenceTextField.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(didEnterBackground:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSString *text = [[PrefHelper shared] getSequenceTextFieldValue];
    if (text) {
        self.sequenceTextField.text = text;
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self saveSequenceTextFieldValue];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (isFirstAppear) {
        [self showSplashScreenIfNeeded];
    }
    
    isFirstAppear = NO;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)didEnterBackground: (NSNotification *) notification {
    [self saveSequenceTextFieldValue];
}

- (void)saveSequenceTextFieldValue {
    NSString *text = self.sequenceTextField.text;
    [[PrefHelper shared] setSequenceTextFieldValue:text];
}

- (SplashScreenVC *)createSplashSreenVC {
    SplashScreenVC *result = [[SplashScreenVC alloc] init];
    return result;
}

- (void)showSplashScreenIfNeeded {
    
    BOOL skipHelp = [[PrefHelper shared] getSkipHelpOnLoad];
    if (!skipHelp) {
        SplashScreenVC *vc = [self createSplashSreenVC];
        UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController:vc];
        [navigationVC setNavigationBarHidden:YES];
        [self presentViewController:navigationVC animated:YES completion:nil];
    }
    
}

- (void)resetResult {
    self.resultLabel.text = @"";
}

- (IBAction)analizeButtonDidTap:(id)sender {
    
    [self.view endEditing:YES];
    
    NSString *text = self.sequenceTextField.text;
    
    NSArray<NSString *> *rawComponents = [text componentsSeparatedByString:@" "];
    
    NSMutableArray<NSString *> *components = [NSMutableArray array];
    for (NSString *element in rawComponents) {
        if (element.length > 0) {
            [components addObject:element];
        }
    }
    
    BOOL isIntegerSequence = YES;
    for (NSString *element in components) {
        if (![self allDigitsInString:element]) {
            isIntegerSequence = NO;
            break;
        }
    }
    
    if (components.count < 3) {
        isIntegerSequence = NO;
    }
    
    if (isIntegerSequence) {
        
        NSMutableArray *numbers = [NSMutableArray array];
        for (NSString *element in components) {
            NSNumber *number = [NSNumber numberWithInteger:[element integerValue]];
            [numbers addObject:number];
        }
        
        if ([self isConstantSequenceOfNumbers:numbers]) {
            self.resultLabel.text = @"Постоянная последовательность";
        } else if ([self isArithmeticSequenceOfNumbers:numbers]) {
            self.resultLabel.text = @"Арифметическая последовательность";
        } else if ([self isGeometricSequenceOfNumbers:numbers]) {
            self.resultLabel.text = @"Геометрическая последовательность";
        } else {
            self.resultLabel.text = @"Произвольная последовательность";
        }
        
    } else {
        self.resultLabel.text = @"Не последовательность";
    }
    
}

- (BOOL)allDigitsInString:(NSString *)input {
    
    BOOL result = NO;
    
    NSString *testString = [NSString string];
    NSScanner *scanner = [NSScanner scannerWithString:input];
    NSCharacterSet *skips = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
    [scanner scanCharactersFromSet:skips intoString:&testString];
    
    if([input length] == [testString length]) {
        result = YES;
    }
    
    return result;
    
}

#pragma mark - Math

- (BOOL)isConstantSequenceOfNumbers:(NSArray *)numbers {
    
    BOOL result = YES;
    
    NSNumber *firstNumber = [numbers objectAtIndex:0];
    
    for (NSNumber *number in numbers) {
        if ([number isEqual:firstNumber] == NO) {
            result = NO;
            break;
        }
    }
    
    return result;
}

- (BOOL)isArithmeticSequenceOfNumbers:(NSArray *)numbers {
    
    BOOL result = YES;
    
    NSInteger delta = [[numbers objectAtIndex:1] integerValue] - [[numbers objectAtIndex:0] integerValue];
    
    for (NSInteger i = 2; i < numbers.count; i++) {
        NSInteger currentDelta = [[numbers objectAtIndex:i] integerValue] - [[numbers objectAtIndex:(i - 1)] integerValue];
        if (currentDelta != delta) {
            result = NO;
            break;
        }
    }
    
    return result;
}

- (BOOL)isGeometricSequenceOfNumbers:(NSArray *)numbers {
    
    BOOL result = YES;
    
    NSInteger int1 = [[numbers objectAtIndex:0] integerValue];
    NSInteger int2 = [[numbers objectAtIndex:1] integerValue];
    
    if (int1 != 0 && int2 != 0) {
        double q = (double) int2 / int1;
        for (NSInteger i = 2; i < numbers.count; i++) {
            NSInteger nextInt = [[numbers objectAtIndex:i] integerValue];
            NSInteger currentInt = [[numbers objectAtIndex:(i - 1)] integerValue];
            if (nextInt != (NSInteger) (q * currentInt)) {
                result = NO;
                break;
            }
        }
    } else {
        result = NO;
    }
    
    return result;
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [self resetResult];
    
    return YES;
}

@end
