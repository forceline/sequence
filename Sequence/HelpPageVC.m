//
//  HelpPageVC.m
//  Sequence
//
//  Created by Artur on 12.10.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import "HelpPageVC.h"

@interface HelpPageVC ()

@end

@implementation HelpPageVC

- (instancetype)initWithTransitionStyle:(UIPageViewControllerTransitionStyle)style navigationOrientation:(UIPageViewControllerNavigationOrientation)navigationOrientation options:(NSDictionary<NSString *,id> *)options {
    
    self = [super initWithTransitionStyle:style navigationOrientation:navigationOrientation options:options];
    if (self) {
        [self calculatePageCount];
    }
    
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addPageControlView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addPageControlView {
    
    UIPageControl *pageControlView = [[UIPageControl alloc] init];
    pageControlView.frame = CGRectMake(10,10,100,100);
    pageControlView.numberOfPages = self.pageCount;
    pageControlView.currentPage = 0;
    pageControlView.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControlView.pageIndicatorTintColor = [UIColor grayColor];
    pageControlView.userInteractionEnabled = NO;
    [self.view addSubview:pageControlView];
    [self.view bringSubviewToFront:pageControlView];
    
    self.pageControlView = pageControlView;
    
    NSDictionary * pageControlViewDic = NSDictionaryOfVariableBindings(pageControlView);
    pageControlView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSArray * hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-80-[pageControlView]-80-|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:pageControlViewDic];
    [self.view addConstraints:hConstraints];
    
    NSArray * vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[pageControlView(20)]-70-|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:pageControlViewDic];
    [self.view addConstraints:vConstraints];

}

- (void)calculatePageCount {
    
    NSInteger pageNumber = 1;
    while (1) {
        NSData *data = [self getTextDataForPageNumber:pageNumber];
        if (data) {
            pageNumber = pageNumber + 1;
        } else {
            _pageCount = pageNumber - 1;
            break;
        }
    }
    
}

- (NSData *)getTextDataForPageNumber:(NSInteger)pageNumber {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"help_page_text_0%ld",(long)pageNumber] ofType:@"txt"];
    
    NSData *result = [NSData dataWithContentsOfFile:filePath];
    
    return result;
    
}

- (UIImage *)getImageForPageNumber:(NSInteger)pageNumber {
    
    UIImage *result = [UIImage imageNamed:[NSString stringWithFormat:@"help_page_picture_0%ld",(long)pageNumber]];
    
    return result;
    
}

- (NSDictionary *)getDataForPageIndex:(NSInteger)pageIndex {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    NSInteger pageNumber = pageIndex + 1;
    
    NSData *textData  = [self getTextDataForPageNumber:pageNumber];
    UIImage *image = [self getImageForPageNumber:pageNumber];
    
    if (textData) {
        [result setObject:textData forKey:@"text"];
    }
    
    if (image) {
        [result setObject:image forKey:@"image"];
    }
    
    return result;
    
}

@end
