//
//  HelpPageVC.h
//  Sequence
//
//  Created by Artur on 12.10.17.
//  Copyright © 2017 Artur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpPageVC : UIPageViewController

@property (readonly, nonatomic) NSInteger pageCount;
@property (weak, nonatomic) UIPageControl *pageControlView;

- (NSDictionary *)getDataForPageIndex:(NSInteger)pageIndex;

@end
